package com.afs.restapi.repository;

import com.afs.restapi.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    public List<Employee> findAllByGender(String gender);

    public List<Employee> findByAgeLessThan(Integer ageLessThan);

    public List<Employee> findByAgeBetween(Integer minAge, Integer maxAge);

    public List<Employee> findByNameContaining(String name);

    public List<Employee> findByCompanyId(Long id);

    @Query("select name from Employee where companyId = ?1")
    public String findEmployeeNameByCompanyId(Long companyId);

    @Query(value = "select name from Employee where companyId = ?1", nativeQuery = true)
    public String findEmployeeNameByCompanyId2(Long companyId);
}
